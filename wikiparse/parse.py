#!/usr/bin/env python
import sys
import os
import re
from datetime import datetime
from collections import defaultdict

from dateutil.relativedelta import relativedelta
from lxml import etree
from matplotlib import pyplot
from matplotlib.dates import date2num
from mwparser import WikiMarkup
from nltk import clean_html
from nltk.stem.snowball import GermanStemmer


WORD_RE = re.compile(r'\w+', re.UNICODE|re.IGNORECASE)


def main():
    now = datetime.now()
    page_dates = []
    revision_dates = []

    corpora = defaultdict(lambda: set())

    stemmer = GermanStemmer()

    for page, revisions in MediawikiDump(sys.stdin).iterpages():
        timestamp = revisions[0]['timestamp']
        page_dates.append(timestamp)

        first = revisions[0]
        stems = set()
        for year in xrange(first['timestamp'].year, now.year+1):
            revisions_in_year = [r for r in revisions if r['timestamp'].year == year]
            revision_dates.extend(r['timestamp'] for r in revisions_in_year)
            if revisions_in_year:
                stems = set()
                for revision in revisions_in_year:
                    html = WikiMarkup(revision['text'].encode('utf-8')).render()
                    text = clean_html(html.decode('utf-8'))
                    # TODO: remove remaining markup
                    words = WORD_RE.findall(text)
                    stems.update(stemmer.stem(word) for word in words)
            corpora[year].update(stems)

    page_dates.sort()
    revision_dates.sort()

    delta = relativedelta(revision_dates[-1], revision_dates[0])
    months = delta.years * 12 + delta.months

    outdir = os.path.abspath('./out')
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    ax.plot_date(page_dates, range(1, len(page_dates)+1), '-')
    ax.hist(date2num(revision_dates), months, histtype='step')
    ax.set_xlabel(u'Year')
    ax.legend([u'Total No. of Pages', u'New Revisions per month'])
    fig.autofmt_xdate()
    fig.savefig(os.path.join(outdir, 'pages.png'), format='png')

    fig = pyplot.figure()
    ax = fig.add_subplot(111)
    timestamps = [datetime(year, 12, 31) for year in sorted(corpora.keys())]
    counts = [len(corpora[t.year]) for t in timestamps]
    ax.plot_date(timestamps, counts, '-')
    ax.set_xlabel(u'Year')
    ax.legend([u'No. of distinct tokens'])
    fig.autofmt_xdate()
    fig.savefig(os.path.join(outdir, 'tokens.png'), format='png')

    years = sorted(corpora.keys())
    years = range(years[0], years[-1])
    year_pairs = zip(years, years[1:])
    for year1, year2 in year_pairs:
        current = corpora.get(year1, set())
        next_ = corpora.get(year2, set())
        filename = '{:04d}-{:04d}.diff'.format(year1, year2)
        with open(os.path.join(outdir, filename), 'w') as f:
            for token in sorted(current - next_):
                f.write(u'-{}\n'.format(token).encode('utf-8'))
            for token in sorted(next_ - current):
                f.write(u'+{}\n'.format(token).encode('utf-8'))


class MediawikiDump(object):
    ENCODING = 'utf-8'
    NAMESPACE = '{http://www.mediawiki.org/xml/export-0.8/}'

    def __init__(self, file_):
        self.file_ = file_

    def iterpages(self):
        context = etree.iterparse(
            self.file_, events=('start', 'end'),
            tag='{}page'.format(self.NAMESPACE),
            encoding=self.ENCODING)
        context = iter(context)

        _, root = next(context)

        for event, page in context:
            if event == 'start':
                continue
            pagedata = {}
            for key in ('id', 'title'):
                pagedata[key] = self.findtext(page, key)

            revisions = []
            for revision in page.iterfind('{}revision'.format(self.NAMESPACE)):
                revisiondata = {}
                for key in ('id', 'comment', 'text'):
                    revisiondata[key] = self.findtext(revision, key)
                timestamp = self.findtext(revision, 'timestamp')
                revisiondata['timestamp'] = self.parse_iso8601(timestamp)
                revisions.append(revisiondata)

            revisions.sort(key=lambda rev: rev['timestamp'])

            # XXX: why is root.clear() not sufficient?
            page.clear()
            while page.getprevious() is not None:
                del page.getparent()[0]
            root.clear()

            yield pagedata, revisions

    def findtext(self, element, attr):
        attr = '{}{}'.format(self.NAMESPACE, attr)
        return element.findtext(attr)

    def parse_iso8601(self, timestamp):
        return datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')

if __name__ == '__main__':
    main()
