#include <stdio.h>
#include <time.h>
#include <err.h>

#define BRIGHTNESS_FILE	"/sys/class/leds/tpacpi::thinklight/brightness"
#define BRIGHTNESS_ON	255
#define BRIGHTNESS_OFF	0
#define MSECS		250

int main(void)
{
	FILE	*f;
	struct timespec	req = {.tv_sec = 0, .tv_nsec = MSECS*1000000};

	f = fopen(BRIGHTNESS_FILE, "w");
	if (f == NULL)
		err(1, "fopen(\"%s\")", BRIGHTNESS_FILE);

	fprintf(f, "%u\n", BRIGHTNESS_ON);
	fflush(f);
	nanosleep(&req, NULL);
	fprintf(f, "%u\n", BRIGHTNESS_OFF);
	fclose(f);
	return (0);
}
